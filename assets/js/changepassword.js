// Change Password Function
function changePasswordFunction() {
	let username = document.getElementById('username').value
	let newPassword = document.getElementById('passwordNew').value
	let confirmPassword = document.getElementById('passwordConfirm').value

	username = username.toUpperCase()
	

	if(newPassword !== confirmPassword) {
		alert('New Password and Confirm Password did not matched!')
		window.location.reload()
	} else {
		fetch(`https://thawing-sea-37347.herokuapp.com/api/users/changePassword/${username}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				password: newPassword
			})
		}).then(res => {
			alert('Successfully changed your password!')
			window.location.replace('./index.html')
		})
	}
}

document.getElementById('changePasswordButton').onclick = function(e) {
	e.preventDefault()
	changePasswordFunction()
}
