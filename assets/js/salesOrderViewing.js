// Retrieving Sales Quote No to View
let params = new URLSearchParams(window.location.search)
let docId = params.get('documentId')

// Global Variables
let token = localStorage.getItem("token")
let accountNameId = ''
let shipToId = ''
let termsId = ''
let taxIdentifier = '';
let productArray = []

let accountNameContainer = document.getElementById('accountName')
let docStatusContainer = document.getElementById('status')
let shipToContainer = document.getElementById("shipTo")
let salesOrderNoContainer = document.getElementById('salesOrderId')
let paymentTermsContainer = document.getElementById("paymentTerms");
let creationDateContainer = document.getElementById('createdOn');
let externalReferenceContainer = document.getElementById('externalReference')
let currencyContainer = document.getElementById("currency");
let requestedDateContainer = document.getElementById('requestedDate')
let sapSoNumberContainer = document.getElementById('sapSoNumber')
let commentContainer = document.getElementById('comment')
let employeeNameContainer = document.getElementById('requestor')
let employeeIdContainer = document.getElementById('employeeId')

let productContainer = document.getElementById('product');
let productDescriptionContainer = document.getElementById("productDescription");
let quantityContainer = document.getElementById('quantity')
let uomContainer = document.getElementById("uom");
let listPriceContainer = document.getElementById("listPrice");
let discountContainer = document.getElementById('discount')
let netPriceContainer = document.getElementById('netPrice');
let taxAmountContainer = document.getElementById('taxAmount')
let netValueContainer = document.getElementById('netValue')



// Sales Quote No.
let documentIdContainer = document.getElementById('documentId')
documentIdContainer.innerHTML = docId

// Logout function
function logoutFunction() {
	localStorage.clear();
	window.location.replace('./logout.html')
}

document.getElementById('userIcon').onclick = function() {
	logoutFunction();
}
 
// Populating Fields

let itemContainer = document.getElementById("existingProduct");

fetch(`https://thawing-sea-37347.herokuapp.com/api/salesOrder/salesOrderMonitoring/${docId}`, {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
}).then(res => res.json()).then(data => {

	accountNameId = data.accountId
	shipToId = data.shipToPartyId
	termsId = data.paymentTermsId
	accountNameContainer.value = data.accountName
	docStatusContainer.value = data.docStatus
	shipToContainer.value = data.shipToPartyDescription
	salesOrderNoContainer.value = data.salesOrderNo
	paymentTermsContainer.value = data.paymentTerms
	creationDateContainer.value = data.creationDate
	externalReferenceContainer.value = data.externalReference
	currencyContainer.value = data.currency
	requestedDateContainer.value = data.requestedDate
	sapSoNumberContainer.value = data.sapSoId
	commentContainer.value = data.comments
	employeeNameContainer.value = data.requestor
	employeeIdContainer.value = data.employeeId

	let productContainer = document.getElementById("productTableContainer")
	let items;	
	items = data.items.map(result => {
		productArray.push(result)
		return (  
			`
			<tr>
				<td style="text-align: center;">${productArray.length}</td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.productId}" disabled></td>
				<td><input type="text" style="width: 100% text-align: center;" value="${result.productDescription}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.quantity}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.unitOfMeasure}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.listPrice}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.discount}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.netPrice}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.taxAmount}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.netValue}" disabled></td>
				<td>
					<div class="d-flex justify-content-center">
						<button class="crudButton button btn ms-1 removeButton" id="removeButton" onclick="remove_tr(this)" disabled>
							<i class="far fa-trash-alt"></i>
						</button>
					</div>
				</td>
			</tr>
			`
		)
	}).join("")
	productContainer.innerHTML = items;
});

// Remove Line Item Function
function remove_tr(This) {
	productArray.splice(This.closest('tr').rowIndex - 1, 1);
		This.closest('tr').remove();
		console.log(productArray)
}

// Buttons
let buttonContainer = document.getElementById("buttonRow")
let docStatus = document.getElementById('status')

if(localStorage.getItem("isApprover") === "true") {
	buttonContainer.innerHTML = 
	`
	<button id="approveButton">Approve</button>
	<button id="rejectButton">Reject</button>
	<button id="closeButton">Close</button>
 	`

	document.getElementById("approveButton").onclick = function(e) {
		e.preventDefault()
		if(docStatus.value === 'Approved') {
			alert(`Sales Quote is already ${docStatus.value}`)
		} else if(docStatus.value === 'Rejected') {
			alert(`Sales Quote is already ${docStatus.value}`)
		} else {
			approveFunction()
		}
	}

	document.getElementById("rejectButton").onclick = function(e) {
		e.preventDefault()
		if(docStatus.value === 'Approved') {
			alert(`Sales Quote is already ${docStatus.value}`)
		} else if(docStatus.value === 'Rejected') {
			alert(`Sales Quote is already ${docStatus.value}`)
		} else {
			rejectFunction()
		}
	}

	document.getElementById("closeButton").onclick = function(e) {
		e.preventDefault()
		closeFunctionApprover()
	}
};

// Approve Function
function approveFunction() {
	fetch(`https://mysterious-shelf-81806.herokuapp.com/api/salesOrder/salesOrderMonitoring/approve/${docId}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(result => {
		alert('Successfully Approved the Sales Quote')
		window.location.reload()
	})
}

// Reject Function
function rejectFunction() {
	fetch(`https://mysterious-shelf-81806.herokuapp.com/api/salesOrder/salesOrderMonitoring/reject/${docId}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(result => {
		alert('Successfully Rejected the Sales Quote')
		window.location.reload()
	})
}

// Close Function
function closeFunctionApprover(){
	window.location.replace('./salesOrderMonitoringFA.html')
}

let buttonContainerView = document.getElementById("buttonRow")

if(localStorage.getItem("isRequestor") === "true") {

	buttonContainerView.innerHTML =
	`
	<button id="newButton">New</button>
	<button id="saveButton">Save</button>
	<button id="editButton">Edit</button>
	<button id="approvalButton">Submit for Approval</button>
	<button id="postButton">Post</button>	
	<button id="closeButton">Close</button>
	`

	document.getElementById('newButton').onclick = function(e) {
		e.preventDefault();
		newFunction();
	}

	document.getElementById('saveButton').onclick = function(e) {
		e.preventDefault();
		saveFunction();
	};

	document.getElementById('editButton').onclick = function(e) {
		e.preventDefault();
		if(docStatusContainer.value === 'In Preparation') {
			editFunction()
		} else if(docStatusContainer.value === 'Returned') {
			editFunction()
		} else {
			alert(`Error! Purchase Request is already ${docStatusContainer.value}`)
		}
	};

	document.getElementById('approvalButton').onclick = function(e) {
		e.preventDefault();
		if(docStatusContainer.value === 'In Preparation') {
			approvalFunction()
		} else {
			alert(`Error! Sales Quote is already ${docStatusContainer.value}`)
		}
	}

	document.getElementById('postButton').onclick = function(e){
		e.preventDefault()
		postFunction()
	}

	document.getElementById("closeButton").onclick = function(e) {
		e.preventDefault();
		closeFunction();
	}
}
	
// New Function
function newFunction() {
	window.location.replace('./salesOrderCreation.html')
}

// Save Function
function saveFunction() {
	accountNameContainer.setAttribute('readonly', true)
	shipToContainer.setAttribute('readonly', true)
	paymentTermsContainer.setAttribute('readonly', true)
	externalReferenceContainer.setAttribute('readonly', true)
	requestedDateContainer.setAttribute('readonly', true)
	commentContainer.setAttribute('readonly', true)
	currencyContainer.setAttribute('readonly', true)
	productContainer.setAttribute('readonly', true)
	document.getElementById("addItems").setAttribute('disabled', true);

	// Setting disabled attribute into old product array
	let oldItems = document.getElementById("productTableContainer");
	let oldRemove = oldItems.getElementsByTagName("button");

	for(let i = 0; i < oldRemove.length; i++) {
		oldRemove[i].setAttribute('disabled', true)
	}

	fetch(`https://thawing-sea-37347.herokuapp.com/api/salesOrder/salesOrderMonitoring/edit/${docId}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			salesOrderNo: salesOrderNoContainer.value,
			accountId: accountNameId,
			accountName: accountNameContainer.value,
			shipToPartyId: shipToId,
			shipToPartyDescription: shipToContainer.value,
			paymentTermsId: termsId,
			paymentTerms: paymentTermsContainer.value,
			requestedDate: requestedDateContainer.value,
			externalReference: externalReferenceContainer.value,
			comments: commentContainer.value,
			docStatus: docStatusContainer.value,
			creationDate: creationDateContainer.value,
			requestor: employeeNameContainer.value,
			employeeId: employeeIdContainer.value,
			currency: currencyContainer.value,
			sapSoId: sapSoNumberContainer,
			items: productArray
		})
	}).then(() => {
		alert(`Successfully Edited the Sales Quote ${docId}!`)
	})
}

// Edit Function
function editFunction() {
	accountNameContainer.removeAttribute('readonly')
	shipToContainer.removeAttribute('readonly')
	paymentTermsContainer.removeAttribute('readonly')
	externalReferenceContainer.removeAttribute('readonly')
	requestedDateContainer.removeAttribute('readonly')
	commentContainer.removeAttribute('readonly')
	currencyContainer.removeAttribute('readonly')
	productContainer.removeAttribute('readonly')
	document.getElementById("addItems").removeAttribute('disabled')
	document.getElementById("removeButton").removeAttribute('disabled')

	// Removing disabled attribute into old product array
	let oldItems = document.getElementById("productTableContainer");
	let oldRemove = oldItems.getElementsByTagName("button");

	for(let i = 0; i < oldRemove.length; i++) {
		oldRemove[i].removeAttribute('disabled')
	}

	// Change Status to In Preparation
	let inStatus = document.getElementById("status")
	inStatus.value = 'In Preparation'

	//Look Up - Account Name
	let accountsDatalist = document.getElementById("accounts")
	let account;

	fetch('https://thawing-sea-37347.herokuapp.com/api/customer/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		account = data.map(result => {
			return  (
				account =
				`
				<option value="${result.accountName}">
				${result.accountId}
				</option>
				`
			)
		})
		accountsDatalist.innerHTML = account;
	}) 

	//Populate Fields - Ship To Location, Payment Terms, Currency, Tax Identifier
	function populateCustomerFields() {
		fetch(`https://thawing-sea-37347.herokuapp.com/api/customer/view/specific/${accountNameContainer.value}`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(data => {
			accountNameId = data.accountId
			shipToId = data.shipToPartyId
			shipToContainer.value = data.shipToPartyDescription
			termsId = data.paymentTermsId
			paymentTermsContainer.value = data.paymentTerms
			currencyContainer.value = data.currency
			taxIdentifier = data.taxStatus
		})
	}

	//Look Up - Currency
	function currencyLookUp() {
		let currencyDatalist = document.getElementById('currencies')
		let curr;
		fetch('https://thawing-sea-37347.herokuapp.com/api/currency/view/all', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(data => {
			curr = data.map(result => {
				return  (
					curr =
					`
					<option value="${result.currencyTicker.toUpperCase()}">
					${result.currencyName}
					</option>
					`
				)
			})
			currencyDatalist.innerHTML = curr;
		}) 
	}

	// Look Up - Payment Terms
	function paymentTermsLookUp() {
		let termsDatalist = document.getElementById('terms')
		let terms;

		fetch('https://thawing-sea-37347.herokuapp.com/api/terms/view/all', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(data => {
			// console.log(data);
			terms = data.map(result => {
				return  (
					`
					<option value="${result.paymentTermsDescription}">
					${result.paymentTermsId}
					</option>
					`
				)
			})
			termsDatalist.innerHTML = terms;
		}) 
	}

	// Look Up - Ship To Location
	function shipToLocationCustomerLookUp() {
		let locationDatalist = document.getElementById('locations')
		let location;

		fetch('https://thawing-sea-37347.herokuapp.com/api/shipToLocationCustomer/view/all', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(data => {
			// console.log(data);
			location = data.map(result => {
				return  (
					`
					<option value="${result.shipToPartyDescription}">
					${result.shipToPartyId}
					</option>
					`
				)
			})
			locationDatalist.innerHTML = location;
		}) 
	}

	accountNameContainer.onchange = function() {
		populateCustomerFields()
		currencyLookUp()
		paymentTermsLookUp()
		shipToLocationCustomerLookUp()
	};

	//Look Up - Products
	let productDatalist = document.getElementById('products')
	let product;
	fetch('https://thawing-sea-37347.herokuapp.com/api/products/view/allProducts', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		// console.log(data);
		product = data.map(result => {
			return  (
				`
				<option value="${result.materialId}">
				${result.materialDescription}
				</option>
				`
			)
		})
		productDatalist.innerHTML = product;
	})

	//Populate Fields - Product Description , Unit of Measure, List Price, and Net Price
	function populateProductFields() {
		fetch(`https://thawing-sea-37347.herokuapp.com/api/products/${productContainer.value}`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(data => {
			productDescriptionContainer.value = data.materialDescription
			listPriceContainer.value = data.price
			uomContainer.value = data.uomDescription
			netPriceContainer.value= data.price
		})
	}

	function enableFields() {
		uomContainer.removeAttribute('readonly')
		quantityContainer.removeAttribute('readonly')
		discountContainer.removeAttribute('readonly')
	}

	//Look Up - Unit of Measure
	function uomLookUp() {
		let uomDatalist = document.getElementById('uoms')
		let uom;
		fetch('https://thawing-sea-37347.herokuapp.com/api/uom/view/all', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(data => {
			uom = data.map(result => {
				return  (
					uom =
					`
					<option value="${result.description}">
					${result.code}
					</option>
					`
				)
			})
			uomDatalist.innerHTML = uom;
		})
	}

	productContainer.oninput = function() {
		populateProductFields()
		enableFields()
		uomLookUp()
	};

	// Generating Net Value
	function generateNetValue() {
		let quantityValue = document.getElementById('quantity').value
		let netValueContainer = document.getElementById('netValue')
		let listPriceValue = document.getElementById('listPrice').value

		netValueContainer.value = quantityValue * listPriceValue
	};

	quantityContainer.oninput = function() {
		generateNetValue()
	};

	// Changing the Net Price based on Discount Field
	function applyDiscount(discountValue) {
		let netPriceValue = document.getElementById("netPrice").value

		discountValue = discountValue/100
		netPriceValue = netPriceValue * (1 - discountValue)

		changeNetPrice(netPriceValue)
	}

	function changeNetPrice(netPriceValue) {
		let netPriceContainer = document.getElementById("netPrice")
		let netValueContainer = document.getElementById("netValue")
		let quantityValue = document.getElementById("quantity").value

		netPriceContainer.value = netPriceValue
		netValueContainer.value = quantityValue * netPriceContainer.value

		calculateTaxAmount();
	}

	function calculateTaxAmount() {
		let netValueValue = document.getElementById("netValue").value
		let taxAmountContainer = document.getElementById("taxAmount")

		if(taxIdentifier === 'Vatable') {
			taxAmountContainer.value = netValueValue * 0.12
		} else {
			taxAmountContainer.value = netValueValue * 0
		}

		itemsPush();
		itemsDisplay();
		clearFunction();
	}

	//Adding Items into Old Array
	function itemsPush() {	
		productArray.push({
			productId: productContainer.value,
			productDescription: productDescriptionContainer.value,
			quantity: quantityContainer.value,
			unitOfMeasure: uomContainer.value,
			listPrice: listPriceContainer.value,
			discount: discountContainer.value,
			netPrice: netPriceContainer.value,
			taxAmount: taxAmountContainer.value,
			netValue: netValueContainer.value,
		})
	}

	// Add Row
	let productTableContainer = document.getElementById("productTableContainer");
	let items;

	function itemsDisplay(){
		let a = 0;
		items = productArray.map(itemsList=> {
			return(
				a++,
				`
				<tr>
					<td>${a}</td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.productId}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.productDescription}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.quantity}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.unitOfMeasure}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.listPrice}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.discount}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.netPrice}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.taxAmount}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.netValue}" disabled></td>
					<td>
					<div class="d-flex justify-content-center">
						<button class="crudButton button btn ms-1" type="button" onclick="remove_tr(this)">
							<i class="far fa-trash-alt"></i>
						</button>
					</div>
				</td>
				</tr>
				`
			)
		}).join("");
		productTableContainer.innerHTML = items; 
	}

	// To Clear Look Ups
	function clearFunction() {
		productContainer.value = ''
		productDescriptionContainer.value = ''
		quantityContainer.value = ''
		uomContainer.value = ''
		listPriceContainer.value = ''
		discountContainer.value = ''
		netPriceContainer.value = ''
		taxAmountContainer.value = ''
		netValueContainer.value = ''
	}

	function disableFields() {
		uomContainer.setAttribute('readonly', true)
		quantityContainer.setAttribute('readonly', true)
		discountContainer.setAttribute('readonly', true)
	}

	document.getElementById("addItems").onclick = function(e) {
		e.preventDefault();
		let discountValue = discountContainer.value
		applyDiscount(discountValue);
		disableFields();
	};

	// Remove Functions
	function remove_tr(This) {
		itemsArray.splice(This.closest('tr').rowIndex - 1, 1);
		This.closest('tr').remove();
	}
}

// Approval Function
function approvalFunction() {
	fetch(`https://thawing-sea-37347.herokuapp.com/api/salesOrder/salesOrderMonitoring/submit/forApproval/${docId}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(result => {
		alert('Successfully Submitted for Approval')
		window.location.reload();
	})
}

// Post Function Status Marker
function postFunction() {
	let accountNameValue = accountNameContainer.value
	let shipToValue = shipToContainer.value
	let paymentTermsValue = paymentTermsContainer.value
	let requestedDateValue = document.getElementById('requestedDate').value
	let externalReferenceValue = document.getElementById('externalReference').value
	let commentsValue = document.getElementById('comment').value
	let creationDateValue = creationDateContainer.value
	let currencyValue = currencyContainer.value
	let requestorValue = employeeNameContainer.value
	let employeeIdValue = employeeIdContainer.value
	let salesOrderNoValue = salesOrderNoContainer.value

		fetch('https://thawing-sea-37347.herokuapp.com/api/salesOrder/salesOrderMonitoring/post', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				salesOrderNo: salesOrderNoValue,
				accountId: accountNameId,
				accountName: accountNameValue,
				shipToPartyId: shipToId,
				shipToPartyDescription: shipToValue,
				paymentTermsId: termsId,
				paymentTerms: paymentTermsValue,
				requestedDate: requestedDateValue,
				externalReference: externalReferenceValue,
				comments: commentsValue,
				docStatus: 'Posted',
				creationDate: creationDateValue,
				currency: currencyValue,
				requestor: requestorValue,
				employeeId: employeeIdValue,
				items: itemsArray
			})
		}).then(res => {
			alert('Successfully Submitted the Sales Quote/s for Posting')
			window.location.reload()
		})
	}

// Close Function
function closeFunction(){
	window.location.replace('./salesOrderMonitoring.html')	
}